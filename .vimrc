"NeoBundle Scripts-----------------------------
if has('vim_starting')
  if &compatible
    set nocompatible               " Be iMproved
  endif

  " Required:
  set runtimepath+=/Users/thinhnguyen/.vim/bundle/neobundle.vim/
endif

" Required:
call neobundle#begin(expand('/Users/thinhnguyen/.vim/bundle'))

" Let NeoBundle manage NeoBundle
" Required:
NeoBundleFetch 'Shougo/neobundle.vim'

" Add or remove your Bundles here:
NeoBundle 'sheerun/vim-polyglot'
NeoBundle 'Shougo/deoplete.nvim'
NeoBundle 'zchee/deoplete-go'
NeoBundle 'flazz/vim-colorschemes'
NeoBundle 'tpope/vim-surround'
NeoBundle 'tpope/vim-repeat'
NeoBundle 'tpope/vim-commentary'
NeoBundle 'tpope/vim-sleuth'
NeoBundle 'tpope/vim-fugitive'
NeoBundle 'fatih/vim-go'
NeoBundle 'garyburd/go-explorer'
NeoBundle 'ctrlpvim/ctrlp.vim'
NeoBundle 'terryma/vim-expand-region'
NeoBundle 'majutsushi/tagbar'
NeoBundle 'airblade/vim-gitgutter'
NeoBundle 'Yggdroot/indentLine'
NeoBundle 'Raimondi/delimitMate'
NeoBundle 'rking/ag.vim'
NeoBundle 'bling/vim-airline'
NeoBundle 'dhruvasagar/vim-prosession', {'depends': 'tpope/vim-obsession'}
NeoBundle 'davidhalter/jedi-vim'
NeoBundle 'zchee/deoplete-jedi'
NeoBundle 'editorconfig/editorconfig-vim'
NeoBundle 'scrooloose/syntastic'
NeoBundle 'jiangmiao/auto-pairs'

" Required:
call neobundle#end()

" Required:
filetype plugin indent on

" If there are uninstalled bundles found on startup,
" this will conveniently prompt you to install them.
NeoBundleCheck
"End NeoBundle Scripts-------------------------

filetype plugin on
filetype plugin indent on
syntax on
colorscheme Tomorrow-Night-Bright

set splitbelow
set splitright

let g:indentLine_faster = 1
let g:indentLine_color_term = 239

"" Searching
set hlsearch
set incsearch
set ignorecase
set smartcase
nnoremap <silent> <Esc> :nohlsearch<Bar>:echo<CR>

"" Encoding
set bomb
set binary


"" Directories for swp files
set nobackup
set noswapfile

"Faster better stronger
set norelativenumber
set nocursorline
set nocursorcolumn
set lazyredraw
set synmaxcol=180
set re=1
set fillchars=diff:·
set ttyfast
syntax sync minlines=256

"Leader hotkeys:
let mapleader = "\<Space>"
nnoremap <Leader>o :CtrlP<CR>
nnoremap <Leader>w :w<CR>
nnoremap <Leader>q :q<CR>
nnoremap <Leader><Leader> V
"Move windows
nmap <Leader>l <C-w>l
nmap <Leader>h <C-w>h
nmap <Leader>j <C-w>j
nmap <Leader>k <C-w>k
"Split windows
nmap <Leader>s <C-w>s
nmap <Leader>v <C-w>v
"Resize windows
nmap <Leader>+ :res +5<CR>
nmap <Leader>= :res +5<CR>
nmap <Leader>- :res -5<CR>
nmap <F8> :TagbarToggle<CR>
"Tabs
nnoremap L gt
nnoremap H gT
nnoremap T :tabnew<CR>
nnoremap <Leader>1 1gt
nnoremap <Leader>2 2gt
nnoremap <Leader>3 3gt
nnoremap <Leader>4 4gt

"Allow hitting vvvv or C-v to expand/shrink current selected region
vmap v <Plug>(expand_region_expand)
vmap <C-v> <Plug>(expand_region_shrink)

"Remove whitespaces on save
autocmd BufWritePre * :%s/\s\+$//e

"Neovim
if has('nvim')
  "Disable python 2
  let g:loaded_python_provider = 1
  let g:python_host_skip_check = 1
  let g:python3_host_prog = '/usr/local/bin/python3'
endif

"Deoplete.nvim
if has('nvim') && has('python3')
  let g:deoplete#enable_at_startup = 1

  " <TAB>: insert candidate and re-generate popup.
  inoremap <expr> <Tab> pumvisible()? deoplete#mappings#close_popup() : "\<Tab>"

  " <C-h>, <BS>: close popup and delete backword char.
  inoremap <expr><C-h> deoplete#mappings#smart_close_popup()."\<C-h>"
  inoremap <expr><BS>  deoplete#mappings#smart_close_popup()."\<C-h>"
endif

" Vim-go
au FileType go nmap <Leader>r <Plug>(go-run)
au FileType go nmap <leader>b <Plug>(go-build)
au FileType go nmap <leader>t <Plug>(go-test)
au FileType go nmap <leader>c <Plug>(go-coverage)
au FileType go nmap <leader>m <Plug>(go-metalinter)
au filetype go nmap <Leader>i <Plug>(go-info)
au FileType go nmap <Leader>gd <Plug>(go-doc)
au FileType go nmap K <Plug>(go-doc)
au FileType go nmap <Leader>gv <Plug>(go-doc-vertical)
au FileType go nmap <Leader>ds <Plug>(go-def-split)
au FileType go nmap <Leader>dv <Plug>(go-def-vertical)
au FileType go nmap <Leader>dd <Plug>(go-def)
au filetype go nmap <Leader>gr <Plug>(go-rename)
au filetype go nmap <F1> <Plug>(go-info)
au FileType go let $GOPATH = go#path#Detect()

let g:go_term_enabled = 1
let g:go_term_mode = "split"
let g:go_list_type = "quickfix"
let g:go_auto_type_info = 1
let g:go_def_reuse_buffer = 1
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_structs = 0
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 0
let g:go_highlight_array_whitespace_error = 0
let g:go_gorename_prefill = 1
let g:go_fmt_command = "goimports"
let g:go_metalinter_enabled = ["golint", "govet"]

" Jedi
let g:jedi#force_py_version = 3
let g:jedi#use_splits_not_buffers = "winwidth"

" Gotagc + tagbar
let g:tagbar_type_go = {
    \ 'ctagstype' : 'go',
    \ 'kinds'     : [
        \ 'p:package',
        \ 'i:imports:1',
        \ 'c:constants',
        \ 'v:variables',
        \ 't:types',
        \ 'n:interfaces',
        \ 'w:fields',
        \ 'e:embedded',
        \ 'm:methods',
        \ 'r:constructor',
        \ 'f:functions'
    \ ],
    \ 'sro' : '.',
    \ 'kind2scope' : {
        \ 't' : 'ctype',
        \ 'n' : 'ntype'
    \ },
    \ 'scope2kind' : {
        \ 'ctype' : 't',
        \ 'ntype' : 'n'
    \ },
    \ 'ctagsbin'  : 'gotags',
    \ 'ctagsargs' : '-sort -silent'
    \ }

" CtrlP speedup
let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'

" Airline
let g:airline#extensions#tabline#enabled = 1

"Gitgutter
let g:gitgutter_sign_column_always=1

"Syntastic!
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_mode_map = { 'mode': 'active', 'passive_filetypes': ['go', 'python'] }
let g:syntastic_go_checkers = ['gometalinter']
let g:syntastic_javascript_checkers = ['eslint']
let g:syntastic_coffee_checkers = ['coffeelint']
let g:syntastic_error_symbol='✗'
let g:syntastic_warning_symbol='⚠'
let g:syntastic_style_error_symbol = '✗'
let g:syntastic_style_warning_symbol = '⚠'

" Configure spaces
au FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
au BufNewFile,BufRead *.py setlocal ts=4 sts=4 sw=4 textwidth=79 expandtab autoindent fileformat=unix
au BufNewFile,BufRead [Mm]akefile* setlocal noexpandtab
