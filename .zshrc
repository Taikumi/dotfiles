# ZSH
export ZSH=$HOME/.oh-my-zsh
export ZSH_CUSTOM=$HOME/.dotfiles/zsh-custom/
export ZSH_THEME="bira"
plugins=(autojump git zsh-syntax-highlighting history-substring-search docker virtualenvwrapper)
source $ZSH/oh-my-zsh.sh

# User related
export PATH="/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/sbin:/usr/bin"
export MANPATH="/usr/local/man:$MANPATH"

export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
    export VISUAL='vim'
else
    export VISUAL='mvim -v'
fi
export EDITOR="$VISUAL"

# Compilation flags
export ARCHFLAGS="-arch x86_64"

# Auto-completion
fpath=("/usr/local/share/zsh/site-functions" $fpath)

# Initiate brew/pyenv/pyenv-virtualenv/rbenv variables
eval "$(pyenv init - --no-rehash)"
eval "$(rbenv init - --no-rehash)"

# virtualenvwrapper configuration
export WORKON_HOME="/venv"
export VIRTUALENVWRAPPER_VIRTUALENV_ARGS='--no-site-packages'

# AWS Credentials
export AWS_PROFILE="magnetic"

# Chef
export CHEF_PATH="/src/chef-repo"
export CHEF_USER="thinhnguyen"

# Golang
export GO15VENDOREXPERIMENT=1
export GOPATH=$HOME/go
export PATH=$PATH:/usr/local/opt/go/libexec/bin:$GOPATH/bin

# General editor
alias vi=vim

# AutoSSH - disable monitor port
export AUTOSSH_PORT=0

# Remote tmux
function rtmux {
  case "$2" in
    "") autossh -M 0 $1 -t "if tmux -qu has; then tmux -qu attach; else EDITOR=vim tmux -qu new; fi";;
    *) autossh -M 0 $1 -t "if tmux -qu has -t $2; then tmux -qu attach -t $2; else EDITOR=vim tmux -qu new -s $2; fi";;
  esac
}

# Copy public key to remote machine
function authme() {
  ssh "$1" 'mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys' < ~/.ssh/id_rsa.pub
}

# Spark!
alias ipyspark="IPYTHON=1 pyspark"
export SPARK_HOME="/usr/local/Cellar/apache-spark/1.3.0/libexec"
export PYSPARK_SUBMIT_ARGS="--master local[4]"


# Aliases
alias urlencode='python -c "import sys, urllib as ul; print ul.quote_plus(sys.argv[1])"'
alias urldecode='python -c "import sys, urllib as ul; print ul.unquote_plus(sys.argv[1])"'
alias oneline='python -c "import sys; arg = sys.argv[1]; print \"\".join(map(str.strip, arg.split(\"\n\")))"'
alias b64decode='python -c "import sys; s = sys.argv[1]; print s.decode("base64")"'
alias b64encode='python -c "import sys; s = sys.argv[1]; print s.encode("base64")"'
alias gist='gist --private'
sssh (){ ssh -t "$1" 'tmux attach || tmux new || screen -DR'; }
alias mysqltsv='mysql --batch -N'
alias sl='open -a /Volumes/Macintosh\ HD/Applications/Sublime\ Text.app'
alias vim='mvim -v'

# Git aliases
alias gitlog="git log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr)%C(bold blue)<%an>%Creset' --abbrev-commit"
alias gitdiff="git diff"
alias gitstatus="git status -sb"
alias glog="gitlog"
alias gstatus="gitstatus"
alias gs="gitstatus"

# Tmux aliases
alias tl="tmux list-sessions"
alias tc="tmux new-session -s"
alias ta="tmux attach-session -t"
function t() {
  if [ "$1" ]; then
    ta $1 || tc $1;
  else
    print "No session name provided!";
fi
}

# boot2docker
function docker-ip() {
  boot2docker ip 2> /dev/null
}
function docker-container-ip {
    docker inspect --format "{{.NetworkSettings.IPAddress}}" $1
}

function docker-env() {
    if [ "$1" ]; then
        eval $(docker-machine env $1)
        export DOCKER_CLIENT_ARGS="$(docker-machine config $1)"
    else
        print "No docker machine name provided!";
    fi
}

function docker-clean-images {
    docker-env
    docker rmi $(docker images -q --filter "dangling=true")
}

function golang-builder() {
    docker run --rm -v "$(pwd):/src" -v /var/run/docker.sock:/var/run/docker.sock centurylink/golang-builder
}

function clear-dns() {
    sudo dscacheutil -flushcache
    sudo killall -HUP mDNSResponder
}

function local-ip {
    ifconfig | grep "inet " | grep -v "127.0.0.1" | awk '{print $2}'
}

# Gitignore.io
function gi() { curl -L -s https://www.gitignore.io/api/\$@ ;}

# Ctrl-z to return back to foreground
fancy-ctrl-z () {
  if [[ $#BUFFER -eq 0 ]]; then
    BUFFER="fg"
    zle accept-line
  else
    zle push-input
    zle clear-screen
  fi
}
zle -N fancy-ctrl-z
bindkey '^Z' fancy-ctrl-z

export HOMEBREW_GITHUB_API_TOKEN=d450716dc14313367cd6a54cdbee93ff0f1762af

# Use gpg-agent
if [ -f "${HOME}/.gpg-agent-info" ]; then
  . "${HOME}/.gpg-agent-info"
  export GPG_AGENT_INFO
  export SSH_AUTH_SOCK
fi
export GPG_TTY=$(tty)
